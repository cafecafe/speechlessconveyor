
import org.bukkit.Material;
import java.util.HashMap;
import java.util.Map;

public class ConveyorMaterial {
    private final Map<Integer, Material> map = new HashMap<>();
    ConveyorMaterial (){
        map.put(0, Material.QUARTZ_SLAB);
        map.put(1, Material.GOLD_BLOCK);
        map.put(2, Material.QUARTZ_SLAB);
        map.put(3, Material.GOLD_BLOCK);
        map.put(4, Material.QUARTZ_BLOCK);
        map.put(5, Material.GOLD_BLOCK);
        map.put(6, Material.QUARTZ_SLAB);
        map.put(7, Material.GOLD_BLOCK);
        map.put(8, Material.QUARTZ_SLAB);

    }
    public Material getMaterial(int i){
        return map.get(i);
    }
}

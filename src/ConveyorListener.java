import oracle.jrockit.jfr.parser.ChunkParser;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.Plugin;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ConveyorListener  implements Listener
{
    private Plugin plugin = Bukkit.getPluginManager().getPlugin("conveyor");
    private Map<String, Set<Conveyor>> playerConveyorList = new HashMap<>();
    @EventHandler
    public void onBuildConveyor(PlayerInteractEvent event){
        if(event.getClickedBlock() == null) return;
        if(event.getClickedBlock().getBlockData().getMaterial()!= Material.OBSIDIAN) return;
        Location loc = event.getClickedBlock().getLocation();
        Conveyor con = Conveyor.getConveyor(loc);
        if(con ==null) {
            new Conveyor(event.getClickedBlock().getLocation());
//            Conveyor.conveyorList_loc.put(loc,con);
        }
        else if(!con.checkTask()){

            con.runParticleTask(loc);
        }
        else{
            con.showTpPosition(event.getPlayer());
        }
    }
    @EventHandler
    public void onWork(PlayerInteractEvent event){
        if(event.getClickedBlock() == null) return;
        if(event.getClickedBlock().getBlockData().getMaterial()!= Material.EMERALD_BLOCK) return;
        double x = event.getClickedBlock().getLocation().getX();
        double y = event.getClickedBlock().getLocation().getY();
        double z = event.getClickedBlock().getLocation().getZ();
        Location loc = event.getClickedBlock().getLocation();
        if(loc.add(0,-1,0).getBlock().getBlockData().getMaterial() !=Material.QUARTZ_SLAB ) return;
        Conveyor con = Conveyor.getConveyor(loc.add(0,-1,0));
        if(con == null) return;
        Bukkit.getLogger().info("fff");
        con.DetectAndSetReady(true);
    }
    @EventHandler
    public void onJoin(PlayerMoveEvent event){
        double x = event.getPlayer().getLocation().getX();
        double y = event.getPlayer().getLocation().getY();
        double z = event.getPlayer().getLocation().getZ();
    }


}

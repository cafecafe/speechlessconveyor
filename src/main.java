import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin {
    static Plugin plugin ;
    @Override
    public void onEnable() {
        plugin = getServer().getPluginManager().getPlugin("conveyor");
        getServer().getPluginManager().registerEvents(new ConveyorListener(),this);
        getCommand("/conveyor").setExecutor(new ConveyorCommand());
    }
}

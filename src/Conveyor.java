import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;


import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Conveyor {
    static int id = 1;
    private static final ConveyorMaterial convetyorM = new ConveyorMaterial();
    private static final Map< Location,  ParticleTask > conveyorTaskList= new HashMap<>();
    public static Map< Location, Conveyor> conveyorList_loc = new HashMap<>();
    private static final Map< Integer, Location> conveyorid_to_loc = new HashMap<>();
    private static final Map<Location,Location> conveyorMap = new HashMap<>();
    private final Material[] m = new Material[9];
    private int conveyor_id ;
    private final Location conveyor_loc;
    Conveyor(Location loc) {
        conveyor_loc = loc.clone();
        conveyor_id = id++;
        try {
            if (!checkBuild()){
                this.finalize();
                return;
            }
        }catch(Throwable e){

        }
        runParticleTask(loc);
        conveyorList_loc.put(loc,this);
        conveyorid_to_loc.put(conveyor_id,conveyor_loc);
            for (Conveyor map_con: conveyorList_loc.values()
                 ) {
                if(map_con.getLocation() == conveyor_loc) continue;
                conveyorMap.put(conveyor_loc, map_con.getLocation());
                conveyorMap.put( map_con.getLocation(),conveyor_loc);
                break;
            }
    }


    public void runParticleTask(Location loc){
        ParticleTask task = new ParticleTask(loc);
        conveyorTaskList.put(conveyor_loc,task);

    }
    public void cancelParticleTask(Location loc){
        conveyorTaskList.get(loc).cancel();
    }
    public void createMap(String id){
        int num_id = Integer.parseInt(id);
        Location map_loc = conveyorid_to_loc.get( num_id );
        conveyorMap.put(conveyor_loc, map_loc);
    }
    public Location getLocation(){
        return conveyor_loc;
    }
    public void teleport(Collection<Entity> entities){
        if(conveyorMap.size() ==0) return;
        Location loc = conveyorMap.get(conveyor_loc).clone();
        for (Entity ele: entities
             ) {
            double dx = ele.getLocation().getX() - conveyor_loc.getX();
            double dz = ele.getLocation().getZ() -conveyor_loc.getZ();
            ele.teleport(loc.add(dx,1,dz));
        }
        conveyorMap.put(conveyorMap.get(conveyor_loc),conveyor_loc);
    }
    public static Conveyor getConveyor(Location loc){
        if(!conveyorList_loc.containsKey(loc)) return null;
        return conveyorList_loc.get(loc);
    }
    public void showTpPosition(Player player){
        ChatColor tag;
        if( conveyorList_loc.size() <2 ) {
            player.sendMessage(ChatColor.RED + "there are no another conveyor!");
        }
        else{
            String to_id = "null";
            Location tar_loc;
            if( (tar_loc = conveyorMap.get(conveyor_loc))==null ){
                player.sendMessage(ChatColor.RED+"The target was disable!");
            }
            else if(!conveyorMap.containsKey(conveyor_loc)){
                player.sendMessage(ChatColor.RED+"You need set new target!");
            }
            else {
                to_id = "" + Conveyor.getConveyor(tar_loc).getId();
            }

            player.sendMessage(ChatColor.AQUA+"Conveyor "+ conveyor_id+ChatColor.BLUE+", to "+to_id);
            player.sendMessage(ChatColor.GREEN+"~~~~~~~~~~~~~~~~~~~~");
            for (Conveyor con: conveyorList_loc.values()

            ) {
                Location loc = con.getLocation();
                if(con.getId() == conveyor_id){
                     tag = ChatColor.AQUA;
                }
                else if( (con.getId()+"").equals(to_id)  ){
                    tag = ChatColor.BLUE;
                }
                else{
                    tag = ChatColor.LIGHT_PURPLE;
                }
                player.sendMessage(tag+""+con.getId()+": "
                        +ChatColor.WHITE+"x: "+ ChatColor.YELLOW+loc.getX()
                        +ChatColor.WHITE+"y: "+ ChatColor.YELLOW+loc.getY()
                        +ChatColor.WHITE+"Z: "+ ChatColor.YELLOW+loc.getZ());
            }
            player.sendMessage(ChatColor.GREEN+"~~~~~~~~~~~~~~~~~~~~");
        }
    }

    private int getId() {
        return conveyor_id;
    }

    public void DetectAndSetReady(boolean b){
        ParticleTask task = conveyorTaskList.get(conveyor_loc);
        if(task!= null&& b ==true && task.getReady()!= true && conveyorList_loc.size() >1 && conveyorMap.get(conveyor_loc)!=null) {
            task.setReady(true);
        }
    }
    public boolean checkBuild(){
        boolean check = true;
        if(conveyor_loc.getBlock().getBlockData().getMaterial() != Material.OBSIDIAN) return false;
        Location left_top_loc = conveyor_loc.clone().add(-1,-1,-1);
        for (int i = 0; i < 9; i++) {
            int  x = i % 3;
            int z =(i / 3);

            m[i] = left_top_loc.clone().add(x,0,z).getBlock().getBlockData().getMaterial();
        }
        for (int i = 0; i < 9; i++) {
            if(m[i] != convetyorM.getMaterial(i)) {
                check = false;
                break;
            }
        }
        return check;
    }
//    public static boolean firstcheckBuild(Location loc){
//        boolean check = true;
//        if(loc.getBlock().getBlockData().getMaterial() != Material.OBSIDIAN) return false;
//        Bukkit.getLogger().info("111");
//        for (int i = 0; i < 8; i++) {
//            check &= m[i] == ConveyorMaterial.map.get(i) && m[i+1] == ConveyorMaterial.map.get(i+1);
//        }
//        return check;
//    }
    public boolean checkTask(){
        return conveyorTaskList.containsKey(conveyor_loc);
    }

    public void free() {
        //remove conveyor mapping
        if(!conveyorMap.isEmpty()) {
            for (Location loc : conveyorMap.keySet()
            ) {
                conveyorMap.remove(loc, conveyor_loc);

            }
            conveyorMap.remove(conveyor_loc);
        }

        //assign new conveyor mapping which lose mapping;
//        for (int i = 1; i <= conveyorList_loc.size(); i++) {
//            if(conveyorid_to_loc.containsKey(i)){
//                conveyorMap.put(conveyor_loc,conveyorid_to_loc.get(i));
//            }
//        }

        //remove this conveyor info
        conveyorList_loc.remove(conveyor_loc);
        conveyorList_loc.remove(conveyor_id);
        try {
            this.finalize();
        }
        catch (Throwable e){
            e.printStackTrace();
        }
    }
    public void setTargetEffect(boolean end){
        Location tar_loc = conveyorMap.get(conveyor_loc).clone().add(-3.5,0,-3.5);
        Location src_loc = conveyor_loc.clone().add(-3.5,0,-3.5);
        World world = conveyor_loc.getWorld();
        if( end ){
            world.playSound(tar_loc,Sound.BLOCK_BEACON_POWER_SELECT,10,10);
            return;
        }
        for (int i = 0; i < 162; i++) {
            Location l = tar_loc.clone();
            Location k = src_loc.clone();
            int  x = i % 9;
            int z =(i / 9) % 9;
            int y =(i / 81);
            world.spawnParticle(Particle.SNOW_SHOVEL, l.add(x,y,z),50);
            world.spawnParticle(Particle.SNOW_SHOVEL, k.add(x,y,z),50);

        }

    }

}

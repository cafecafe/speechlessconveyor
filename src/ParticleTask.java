import org.bukkit.*;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class ParticleTask extends BukkitRunnable
{
    private Plugin plugin = main.plugin;
    private Location loc ;
    private boolean ready =false;
    private int count =0;
    ParticleTask(Location loc){
        this.loc = loc;
        this.runTaskTimer(plugin,10L,10L);
    }
    public void setReady(boolean b){
        ready = b;
    }
    @Override
    public void run() {
        Conveyor con = Conveyor.getConveyor(loc);
        if(!con.checkBuild()) {
            con.free();
            this.cancel();
        }
        else if(ready){
            count++;
//            Location left_top_loc = loc.clone().add(-1,2,-1);
//            for (int j = 0; j < 16; j++) {
//                double x = left_top_loc.getX() + j %4;
//                double z = left_top_loc.getZ() + j/4;
//                double y =left_top_loc.getY() ;
//                loc.getWorld().spawnParticle(Particle.TOTEM, x, y, z, 5);
//            }
            if(count==10) {
                for(int i =0; i<100;i++){
                    loc.getWorld().spawnParticle(Particle.TOTEM, loc.getX()+0.5,loc.getY()+i,loc.getZ()+0.5, 100);
                }
                Location clone_loc = loc.clone();
                con.teleport(loc.getWorld().getNearbyEntities(loc,5,2,5));
                clone_loc.add(0,2,0).getBlock().setType(Material.AIR);
                ready = false;
                count=0;
                loc.getWorld().playSound(loc, Sound.BLOCK_BEACON_POWER_SELECT,20,10);
                con.setTargetEffect(true);
                new BukkitRunnable(){
                    @Override
                    public void run() {
                        loc.getWorld().playSound(loc, Sound.BLOCK_BEACON_DEACTIVATE,10,10);
                    }
                }.runTaskLater(plugin,20L);
            }else{
                loc.getWorld().playSound(loc, Sound.BLOCK_BEACON_ACTIVATE,10,10);
                con.setTargetEffect(false);
            }
        }
        else{
            for(int i =0;i<100;i++){
                loc.getWorld().spawnParticle(Particle.PORTAL, loc.getX()+0.5, loc.getY() + i, loc.getZ()+0.5, 10);
            }
        }
    }

    public boolean getReady() {
        return ready;
    }
}

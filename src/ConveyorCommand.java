import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ConveyorCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Location loc =null;
        try{
            loc = Bukkit.getPlayer(commandSender.getName()).getTargetBlockExact(5).getLocation();
        }
        catch (NullPointerException e){
            commandSender.sendMessage(ChatColor.RED+"Don't look at air ,dude !");
            return true;
        }
        String id;
        Conveyor con = Conveyor.getConveyor(loc);
        if(con == null || loc == null) {
            commandSender.sendMessage(ChatColor.RED+"You must be near by conveyor and look at OBSIDIAN!!");
            commandSender.sendMessage(ChatColor.RED+"if it's not work ,be sure you have actived it!!");
        }
        else if(strings.length >0){
            id = strings[0];
            commandSender.sendMessage("x:"+loc.getX()+" y:"+loc.getY()+" z:"+loc.getZ());
            con.createMap(id);
            commandSender.sendMessage(ChatColor.GREEN+"Set target successfully ~~");
        }
        else{
            commandSender.sendMessage(ChatColor.YELLOW+"Please type your target number after command!");
        }

        return true;
    }
}
